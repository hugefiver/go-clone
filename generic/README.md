# Generic `go-clone` API

[![Go](https://github.com/huandu/go-clone/workflows/Go/badge.svg)](https://github.com/huandu/go-clone/actions)
[![Go Doc](https://godoc.org/github.com/huandu/go-clone/generic?status.svg)](https://pkg.go.dev/github.com/huandu/go-clone/generic)

This package is a set of generic API for `go-clone`. All methods are simple proxies. It requires `go1.18` or later to build this package.

Please read document in [the main project](../README.md) for more information.
